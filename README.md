## Implementation notes

### General

There was a request to make the whole service available at the 'http://127.0.0.1:3000/' address.
Although it is generaly not a good idea to mix a backend and frontend like this, nevertheless I've configured a Sinatra to serve static files (html/js/css).
I should mention that in a production environment static files should be served by a nginx webserver, that is a faster and a safer way.

For a second I've though about starting a docker nginx image and using a docker-composer to make a complex environment architecturally resembling a real thing.
But then decided against it because that would be certanly an overkill and a waste of time (my and of reviewers too).

Also using a "thin" webserver in production is not a common way. But it is easier to configure and uses less boilerplate.
In a production environment I would use a "unicorn" webserver instead.

### Frontend

I've done some extra - autocompletion (via jQuery and AJAX request to /airport-completion). I think this would improve usability a lot.

I haven't used my JavaScript frontend (jQuery/DOM) skills for a while, so I haven't completed some things. Essentialy it is:

* JSON to Table converting. Currently there is a raw JSON reply from a backend and not a table.
* date[+-][12] tabs should manipulate AjaxSettings to send different date at #BeforeLoad. Currently they all just use requested date with no delta.

### Backend

Code works and is covered with tests. I've used TDD and RSpec, and this is my preferred way to do things.

There should be a slighter better OOP, and code could use some refactoring (essentialy a class extraction to mitigate SRP violations).
Also I could introduce validations/repositories/entities/containers/operations/transactions from a dry-rb stack and make code cleaner and future/business proof (but much more complex from the start).
But then I've decided to keep things simple and not to overengineer them.

If I knew that this would be a seed of some bigger project I could use a Dry-rb, Rom-rb and Faraday and maybe FactoryGirl from the start.
But for this little project that would be an overkill too.

## Outro

Thanks for reading this. Hope you will enjoy a code review.