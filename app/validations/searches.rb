module Locomote
  module Validations
    class Searches
      attr_reader :from, :to, :date
      def initialize params
        @from = params.fetch(:from, nil)
        @to = params.fetch(:to, nil)
        @date = params.fetch(:date, nil)
      end
      def success?
        return false unless @from && @to && @date
        return false if @from.empty? || @to.empty? || @date.empty?
        true
      end
    end
  end
end
