# encoding: utf-8
require 'sinatra'

class MyApp < Sinatra::Application
  enable :sessions

  configure :production do
    set :clean_trace, true
  end
end

Dir[File.join(File.dirname(__FILE__), 'routes', '*.rb')].each do |e|
  require_relative e
end

