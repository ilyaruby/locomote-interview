require 'httparty'
require 'json'
require_relative "airline"

module Locomote
  class Airlines
    def config
      { url: "http://node.locomote.com/code-task/airlines" }
    end
    def airlines
      return [] unless success?
      remote_response.parsed_response.map do |data|
        Locomote::Airline.new data
      end
    end
    def success?
      remote_response.code == 200
    end
    def remote_response
      @remote_response ||= HTTParty.get config[:url]
    end
    def local_response
      remote_response.parsed_response
    end
    def serialize
      local_response.to_json
    end
  end
end
