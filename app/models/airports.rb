require 'httparty'
require 'json'

module Locomote
  class Airports
    def initialize q
      @q = q
      @remote_response
    end
    def config
      { url: "http://node.locomote.com/code-task/airports" }
    end
    def remote_response
      @remote_response ||= HTTParty.get config[:url].concat("?q=#{@q}")
    end
    def remote_response_code
      remote_response.code
    end
    def success?
      remote_response.code == 200
    end
    def local_response
      remote_response.parsed_response.to_json
    end
    def suggestions
      remote_response.parsed_response.map do |item|
        flightapi_airport_to_suggestion item
      end.to_json
    end
    def flightapi_airport_to_suggestion item
      { 
        "value" => item["airportCode"],
        "label" => item["airportCode"] + ": " + item["cityName"] + " - " + item["airportName"]
      }
    end
  end
end
