module Locomote
  class Flight
    attr_reader :json_body
    def initialize json_body
      @json_body = json_body
    end
    def serialize
      json_body
    end
  end
end
