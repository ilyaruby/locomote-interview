require 'httparty'
require 'json'
require_relative '../validations/searches.rb'
require_relative '../models/flight.rb'

module Locomote
  class Searches
    attr_reader :from, :to, :date
    def initialize params
      validate params
      @from = @validation.from
      @to = @validation.to
      @date = @validation.date
    end
    def validate params
      @validation ||= Locomote::Validations::Searches.new params
    end
    def validated?
      @validation.success? 
    end
    def airlines
      @airlines ||= Locomote::Airlines.new.airlines
    end
    def config
      { url: "http://node.locomote.com/code-task/flight_search" }
    end
    def url(airline)
      config[:url].concat("/#{airline.code}?from=#{from}&to=#{to}&date=#{date}")
    end
    def remote_search airline
      @remote_response ||= request_remote_api(airline)
    end
    def request_remote_api airline
      response = HTTParty.get(url(airline))
      return [] unless response.code == 200
      response.parsed_response
    end
    def results
      airlines.map do |airline|
        remote_search(airline).map do |flight|
          Locomote::Flight.new flight
        end
      end.flatten
    end
    def serialize
      results.map(&:serialize).to_json
    end
  end
end
