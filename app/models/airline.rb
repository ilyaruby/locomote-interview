module Locomote
  class Airline
    attr_reader :code, :name
    def initialize data
      @code = data["code"]
      @name = data["name"]
    end
  end
end
