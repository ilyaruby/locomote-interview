require 'sinatra'
require_relative '../models/airlines.rb'

get '/airlines' do
  Locomote::Airlines.new.serialize
end
