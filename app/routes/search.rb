require 'sinatra'
require_relative '../models/searches.rb'

get '/search' do
  results = Locomote::Searches.new(params)
  if results.validated?
    results.serialize
  else
    status 400
    "Error: unvalidated"
  end
end
