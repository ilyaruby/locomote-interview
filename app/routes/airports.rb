require 'sinatra'
require_relative '../models/airports.rb'

get '/airports-suggestions' do
  airports = Locomote::Airports.new(params[:q])
  if airports.success?
    params[:callback] + "(" + airports.suggestions + ");"
  else
    {}
  end
end

get '/airports' do
  airports = Locomote::Airports.new(params[:q])
  if airports.success?
    airports.local_response
  else
    status airports.remote_response_code || 500
    "Error: #{airports.remote_response}"
  end
end
