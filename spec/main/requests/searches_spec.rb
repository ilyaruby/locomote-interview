require 'app_helper'

RSpec.describe :searches, type: :request do
  def app
    Sinatra::Application
  end
  
  describe "Lists matching searches from the Flight API" do
    let(:from) { "SYD" }
    let(:to) { "JFK" }
    let(:date) { "2018-09-02" }

    before :each do
      subject
    end

    subject do
      get "/search?from=#{from}&to=#{to}&date=#{date}"
    end

    it "retuns 201" do
      expect(last_response).to be_ok
    end

    it "returns response body" do
      expect(last_response.body).not_to be_empty
    end

    it "contains searches with codes" do
      expect(json_response).to include(
        including(
          "flightNum"
        )
      )
    end

    context "No query parameter given" do
      let(:from) { "" }

      it "retuns 400" do
        expect(last_response.status).to eq 400
      end
    end
  end
end
