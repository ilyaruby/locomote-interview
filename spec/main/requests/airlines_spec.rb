require 'app_helper'

RSpec.describe :airlines, type: :request do
  def app
    Sinatra::Application
  end
  
  describe "Lists all available airlines from the Flight API" do
    before :each do
      subject
    end

    subject do
      get '/airlines'
    end

    it "retuns 201" do
      expect(last_response).to be_ok
    end

    it "returns response body" do
      expect(last_response.body).not_to be_empty
    end

    it "contains airports with codes" do
      expect(json_response).to include(
        including(
          "code"
        )
      )
    end
  end
end
