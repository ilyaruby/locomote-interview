require 'app_helper'

RSpec.describe :airports, type: :request do
  def app
    Sinatra::Application
  end
  
  describe "Lists matching airports from the Flight API" do
    let(:query) { "Moscow" }

    before :each do
      subject
    end

    subject do
      get "/airports?q=#{query}"
    end

    it "retuns 201" do
      expect(last_response).to be_ok
    end

    it "returns response body" do
      expect(last_response.body).not_to be_empty
    end

    it "contains airports with codes" do
      expect(json_response).to include(
        including(
          "airportCode"
        )
      )
    end

    context "No query parameter given" do
      let(:query) { "" }

      it "retuns 400" do
        expect(last_response.status).to eq 400
      end
    end
  end
end
