require 'app_helper'

RSpec.describe :airlines do
  describe "Lists all available airlines from the Flight API" do
    before :each do
      subject
    end

    context "#airlines" do
      subject do
        Locomote::Airlines.new.airlines
      end

      it "returns a list of airlines" do
        expect(subject).to be_a Array
      end

      it "returns a non-empty list of airlines" do
        expect(subject).not_to be_empty
      end

      it "contains Airlines" do
        expect(subject.first).to be_a Locomote::Airline
      end
    end
  end
end
