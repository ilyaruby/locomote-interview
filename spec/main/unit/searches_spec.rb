require 'app_helper'

RSpec.describe :searches do
  describe "Searches on all airlines for a flight" do
    let(:from) { "SYD" }
    let(:to) { "JFK" }
    let(:date) { "2018-09-02" }

    let(:search_attrs) {
      {
        from: from,
        to: to,
        date: date
      }
    }

    before :each do
      subject
    end

    subject do
      Locomote::Searches.new(search_attrs).results
    end

    it "returns flights" do
      expect(subject).to be_a Array
    end

    it "returns a non-empty list of flights" do
      expect(subject).not_to be_empty
    end

    it "contains Flights" do
      expect(subject.first).to be_a Locomote::Flight
    end
  end
end
