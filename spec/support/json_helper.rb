module JsonApiHelpers                                                                                                                                         
  def json_response                                                                                                                                           
    begin                                                                                                                                                     
      JSON.parse last_response.body                                                                                                                           
    rescue JSON::ParserError => e                                                                                                                             
      binding.pry                                                                                                                                             
    end                                                                                                                                                       
  end                                                                                                                                                         
end
