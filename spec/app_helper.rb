require 'pry'
require 'spec_helper'

SPEC_ROOT = Pathname(__FILE__).dirname                                                                                                                        
Dir[SPEC_ROOT.join("../app/models/*.rb").to_s].each(&method(:require))
Dir[SPEC_ROOT.join("../app/validations/*.rb").to_s].each(&method(:require))
Dir[SPEC_ROOT.join("../app/routes/*.rb").to_s].each(&method(:require))
Dir[SPEC_ROOT.join("support/*.rb").to_s].each(&method(:require))

RSpec.configure do |config|
  config.include Rack::Test::Methods, type: :request
  config.include JsonApiHelpers, type: :request
end
